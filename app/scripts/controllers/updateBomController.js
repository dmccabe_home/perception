'use strict';


angular.module('webApp')
  .controller('UpdateBomCtrl', function ($scope, $modalInstance, item, parts, partMap) {
    $scope.copiedBom = angular.copy(item);
    $scope.parts = parts;
    $scope.partMap = partMap;

    $scope.gridOptions = {
      data : 'copiedBom.parts',
      enableCellSelection: true,
      enableRowSelection: false,
      enableCellEdit: true,
      enableColumnResize: true,
      columnDefs: [
        {field:'partNumber', displayName:'Part Number'},
        {field:'quantity', displayName:'Quantity', enableCellEdit:true},
        {field:'', displayName:'Description', cellTemplate: '<div class="ngCellText">{{partMap[row.entity[\'partNumber\']].description}}</div>', enableCellEdit:false},
        {field:'', displayName:'Type', cellTemplate: '<div class="ngCellText">{{partMap[row.entity[\'partNumber\']].type}}</div>', enableCellEdit:false},
        {field:'', cellTemplate:'<label class="btn btn-sm btn-danger" ng-click="delete(row)" uncheckable>Delete</label>'}]};

    $scope.delete = function (row) {
      var index = row.rowIndex;
      $scope.gridOptions.selectItem(index, false);
      $scope.copiedBom.parts.splice(index, 1);
    }

    $scope.update = function() {
      $modalInstance.close($scope.copiedBom);
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }

    $scope.addSelectedPart = function() {
      if($scope.selectedPart) {
        var part = {
          partNumber: $scope.selectedPart,
          quantity: 1
        }

        $scope.copiedBom.parts.push(part);
      }
    }
  });
