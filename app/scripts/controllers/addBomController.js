'use strict';


angular.module('webApp')
  .controller('AddBomCtrl', function ($scope, $modalInstance, parts, partMap) {
    $scope.bom = {}
    $scope.bom.parts = [];
    $scope.parts = parts;
    $scope.partMap = partMap;

    $scope.gridOptions = {
      data : 'bom.parts',
      enableCellSelection: true,
      enableRowSelection: false,
      enableCellEdit: true,
      enableColumnResize: true,
      columnDefs: [
        {field:'partNumber', displayName:'Part Number'},
        {field:'quantity', displayName:'Quantity', enableCellEdit:true},
        {field:'', displayName:'Description', cellTemplate: '<div class="ngCellText">{{partMap[row.entity[\'partNumber\']].description}}</div>', enableCellEdit:false},
        {field:'', displayName:'Type', cellTemplate: '<div class="ngCellText">{{partMap[row.entity[\'partNumber\']].type}}</div>', enableCellEdit:false},
        {field:'', cellTemplate:'<label class="btn btn-sm btn-danger" ng-click="delete(row)" uncheckable>Delete</label>'}]};

    $scope.delete = function (row) {
      var index = row.rowIndex;
      $scope.gridOptions.selectItem(index, false);
      $scope.bom.parts.splice(index, 1);
    }

    $scope.add = function() {
      $modalInstance.close($scope.bom);
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }

    $scope.addSelectedPart = function() {
      if($scope.selectedPart) {
        var part = {
          partNumber: $scope.selectedPart,
          quantity: 1
        }

        $scope.bom.parts.push(part);
      }
    }
  });
