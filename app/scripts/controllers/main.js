'use strict';


angular.module('webApp')
  .controller('MainCtrl', function ($scope, $http, $modal) {

    $scope.boms = [];
    $scope.parts = [];
    $scope.partMap = []
    $scope.alerts = [];

    var backendHost = "https://salty-savannah-1100.herokuapp.com";
    //var backendHost = "http://localhost:8080";

    $scope.getPartsAndBoms = function() {
      $http.get(backendHost + '/boms')
        .success(function(data) {
          $scope.boms = data;
        })
        .error(function(data, status, headers, config){
          $scope.alerts.push({type: 'danger', message: 'Cannot connect to backend Java service to get BOMs'})
        });

      $http.get(backendHost + '/parts')
        .success(function(data) {
          $scope.parts = data;
          angular.forEach($scope.parts, function(part, key) {
            $scope.partMap[part.corporatePartNumber] = part;
          });
        })
        .error(function() {
          $scope.alerts.push({type: 'danger', message: 'Cannot connect to backend Java service to get Parts'})
        });
    }

    $scope.init = function() {
      $scope.getPartsAndBoms();
    };

    $scope.update = function(bom) {
      $http.put(backendHost + '/boms', bom)
        .success(function() {
          $scope.getPartsAndBoms();
          console.log("Updated bom successfully");
          console.log(bom);
        })
        .error(function() {
          console.log("Problem updating bom")
        });
    };

    $scope.insert = function(bom) {
      $http.post(backendHost + '/boms', bom)
        .success(function() {
          $scope.getPartsAndBoms();
          console.log("Inserted bom successfully");
          console.log(bom);
        })
        .error(function() {
          console.log("Problem inserting bom")
        });

    };

    $scope.delete = function(bom) {
      $http.delete(backendHost + '/boms/' + bom.bomNumber)
        .success(function() {
          $scope.getPartsAndBoms();
          console.log("Deleted bom successfully");
        })
        .error(function() {
          console.log("Problem deleting bom");
        });
    };

    $scope.showBomUpdateDialog = function(bom) {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'views/updateBom.html',
        controller: 'UpdateBomCtrl',
        size: 'lg',
        resolve: {
          item: function () {
            return bom;
          },
          parts: function() {
            return $scope.parts;
          },
          partMap: function() {
            return $scope.partMap;
          }
        }
      });

      modalInstance.result.then(function (updatedBom) {
        var index = _.findIndex($scope.boms, 'bomNumber', updatedBom.bomNumber);
        $scope.update(updatedBom);
        $scope.boms[index] = updatedBom;
      });
    };

    $scope.showBomAddDialog = function() {
      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'views/addBom.html',
        controller: 'AddBomCtrl',
        size: 'lg',
        resolve: {
          parts: function () {
            return $scope.parts;
          },
          partMap: function() {
            return $scope.partMap;
          }
        }
      });

      modalInstance.result.then(function (bom) {
        $scope.insert(bom);
      });
    }

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

    $scope.init();
  });
